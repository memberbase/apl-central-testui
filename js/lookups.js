var lookup_kyc_states = [
  {"id":"level1", "desc":"level1"},
  {"id":"level2", "desc":"level2"},
  {"id":"level3", "desc":"level3"}
];

var transaction_options = [
	{id:1, implimented:true, action:'purchase_voucher',actor_type:'client',fund_type:'wallet',client_id:true,agent_id:false,amount:true,mobile_number:false,voucher_number:false,beneficiary_id:false},
	{id:2, implimented:true, action:'purchase_voucher',actor_type:'agent',fund_type:'wallet',client_id:true,agent_id:true,amount:true,mobile_number:false,voucher_number:false,beneficiary_id:false},
	{id:3, implimented:true, action:'purchase_voucher',actor_type:'agent',fund_type:'cash',client_id:false,agent_id:true,amount:true,mobile_number:true,voucher_number:false,beneficiary_id:false},
	{id:4, implimented:true, action:'purchase_card',actor_type:'client',fund_type:'wallet',client_id:true,agent_id:false,amount:true,mobile_number:false,voucher_number:false,beneficiary_id:false},
	{id:5, implimented:true, action:'purchase_card',actor_type:'client',fund_type:'voucher',client_id:true,agent_id:false,amount:false,mobile_number:false,voucher_number:true,beneficiary_id:false},
	{id:6, implimented:true, action:'purchase_card',actor_type:'agent',fund_type:'wallet',client_id:true,agent_id:true,amount:true,mobile_number:false,voucher_number:false,beneficiary_id:false},
	{id:7, implimented:true, action:'purchase_card',actor_type:'agent',fund_type:'cash',client_id:false,agent_id:true,amount:true,mobile_number:true,voucher_number:false,beneficiary_id:false},
	{id:8, implimented:true, action:'purchase_card',actor_type:'agent',fund_type:'voucher',client_id:false,agent_id:true,amount:false,mobile_number:true,voucher_number:true,beneficiary_id:false},
	{id:9, implimented:true, action:'load_wallet',actor_type:'client',fund_type:'voucher',client_id:true,agent_id:false,amount:false,mobile_number:false,voucher_number:true,beneficiary_id:false},
	{id:10, implimented:true, action:'load_wallet',actor_type:'agent',fund_type:'voucher',client_id:true,agent_id:true,amount:false,mobile_number:false,voucher_number:true,beneficiary_id:false},
	{id:11, implimented:true, action:'load_wallet',actor_type:'agent',fund_type:'cash',client_id:true,agent_id:true,amount:true,mobile_number:false,voucher_number:false,beneficiary_id:false},
	{id:12, implimented:true, action:'send_money',actor_type:'client',fund_type:'wallet',client_id:true,agent_id:false,amount:true,mobile_number:false,voucher_number:false,beneficiary_id:true},
	{id:13, implimented:true, action:'send_money',actor_type:'agent',fund_type:'wallet',client_id:true,agent_id:true,amount:true,mobile_number:false,voucher_number:false,beneficiary_id:true},
	{id:14, implimented:true, action:'send_money',actor_type:'agent',fund_type:'cash',client_id:false,agent_id:true,amount:true,mobile_number:false,voucher_number:false,beneficiary_id:true},
	{id:15, implimented:true, action:'send_money',actor_type:'agent',fund_type:'voucher',client_id:false,agent_id:true,amount:true,mobile_number:false,voucher_number:false,beneficiary_id:true},
	{id:16, implimented:false, action:'withdraw_cash',actor_type:'agent',fund_type:'voucher',client_id:false,agent_id:true,amount:false,mobile_number:false,voucher_number:true,beneficiary_id:false},
	{id:17, implimented:false, action:'withdraw_cash',actor_type:'agent',fund_type:'wallet',client_id:true,agent_id:true,amount:true,mobile_number:false,voucher_number:false,beneficiary_id:false},
];

function init_lookups($rootScope){
	$rootScope.lookup_kyc_states = lookup_kyc_states;
	$rootScope.transaction_options = transaction_options;
}


