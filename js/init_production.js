  main_services.factory('init_service', function($rootScope) {
    var service_functions = {};

    service_functions.init_api_uris = function() {
      $rootScope.api_gen = 'http://aplcentral_production.memberbase.net/api';
    };

    service_functions.init_lookup_selectors = function() {
      init_lookups($rootScope);
    };

    return service_functions;
  });
