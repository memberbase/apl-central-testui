main_controllers.controller('transactions_controller', function($scope, $rootScope, $location, $http) {
	navbar_remove_all_active();
	$("#navli_transactions").addClass("active");

	$scope.rs = $rootScope;
	$scope.ui = {};
	$scope.ui.invalid = {};
	$scope.uidirty = false;

    $scope.ui_select_voucher = '';
    $scope.ui_select_agent = '';
    $scope.ui_select_client = '';
    $scope.ui_select_beneficiary = '';

	$scope.ui_vouchers = [{id:'',desc:'Select'}];
	$scope.ui_agents = [{id:'',desc:'Select'}];
	$scope.ui_clients = [{id:'',desc:'Select'}];
	$scope.ui_beneficiaries = [{id:'',desc:'Select'}];

	$scope.$watch("ui_select_voucher",function() {
		$scope.ui.voucher_number = $scope.ui_select_voucher;
	});
	$scope.$watch("ui_select_agent",function() {
		$scope.ui.agent_id = $scope.ui_select_agent;
	});
	$scope.$watch("ui_select_client",function() {
		$scope.ui.client_id = $scope.ui_select_client;
	});
	$scope.$watch("ui_select_beneficiary",function() {
		$scope.ui.beneficiary_id = $scope.ui_select_beneficiary;
	});

	$scope.submit_button = function(){
		var payload = {};
		if($scope.ui.action && $scope.ui.action.length > 0) payload.action = $scope.ui.action;
		if($scope.ui.actor_type && $scope.ui.actor_type.length > 0) payload.actor_type = $scope.ui.actor_type;
		if($scope.ui.fund_type && $scope.ui.fund_type.length > 0) payload.fund_type = $scope.ui.fund_type;
		if($scope.ui.amount && $scope.ui.amount.length > 0) payload.amount = $scope.ui.amount;
		if($scope.ui.voucher_number & $scope.ui.voucher_number.length > 0) payload.voucher_number = $scope.ui.voucher_number;
		if($scope.ui.agent_id && $scope.ui.agent_id.length > 0) payload.agent_id = $scope.ui.agent_id;
		if($scope.ui.mobile_number && $scope.ui.mobile_number.length > 0) payload.mobile_number = $scope.ui.mobile_number;
		if($scope.ui.client_id && $scope.ui.client_id.length > 0) payload.client_id = $scope.ui.client_id;
		if($scope.ui.beneficiary_id && $scope.ui.beneficiary_id.length > 0) payload.beneficiary_id = $scope.ui.beneficiary_id;

		gen_http(
			$http,
			201,
			{
				method: 'POST',
				url: $rootScope.api_gen + '/transactions',
				params: { 'foobar': new Date().getTime() },
				data: payload
			},
			function (data, status){
				if(status){
					$scope.last_response_status = status;
				}
				if(data){
					$scope.last_response_data = data;
				}
				//gen_http_error(data, status);
			},
			function (data, status){
				//alert('Transaction successfull');
				if(status){
					$scope.last_response_status = status;
				}
				if(data){
					$scope.last_response_data = data;
				}
				get_select_vouchers();
				get_select_agents();
				get_select_clients();
				get_select_beneficiaries();
			}
		);
	}


	var grid_select_template = '<div ng-show="row.entity.implimented" style="text-align:center">';
	grid_select_template += '<button type="button" class="btn btn-xs"';
	grid_select_template += 'ng-click="tx_select_button(row.entity[col.field])">';  
	grid_select_template += '<i class="fa fa-arrow-right"></i>';
	grid_select_template += '</button>';
	grid_select_template += '</div>';

	var grid_tf_template = '<div style="text-align:center;">'
    + '<span ng-show="row.getProperty(col.field)" >'
    + '<i class="fa fa-square"></i>'
    + '</span>'
    // + '<span ng-show="!row.getProperty(col.field)" >'
    // + '<i class="fa fa-square-o"></i>'
    // + '</span>'
    + '</div>';


	$scope.tx_select_button = function(id){
		$scope.ui.action = $scope.transaction_options[id - 1].action;
		$scope.ui.actor_type = $scope.transaction_options[id - 1].actor_type;
		$scope.ui.fund_type = $scope.transaction_options[id - 1].fund_type;
	}

	$scope.transaction_options = $rootScope.transaction_options;

	$scope.grid_cols_def = [
		{field: 'id', displayName: 'TX', width: 40, cellTemplate : grid_select_template, editableCellTemplate : grid_select_template },
		{field: 'id', displayName: 'ID', width: 40},
		{field: 'action', displayName: 'Action', width: 130},
		{field: 'actor_type', displayName: 'Actor Type', width: 100},
		{field: 'fund_type', displayName: 'Fund Type', width: 100},
		{field: 'amount', displayName: 'Amount', width: 80, cellTemplate : grid_tf_template, editableCellTemplate : grid_tf_template },
		{field: 'voucher_number', displayName: 'Voucher #', width: 100, cellTemplate : grid_tf_template, editableCellTemplate : grid_tf_template },
		{field: 'agent_id', displayName: 'Agent ID', width: 80, cellTemplate : grid_tf_template, editableCellTemplate : grid_tf_template },
		{field: 'client_id', displayName: 'Client ID', width: 80, cellTemplate : grid_tf_template, editableCellTemplate : grid_tf_template },
		{field: 'mobile_number', displayName: 'Mobile # / Client ID', width: 140, cellTemplate : grid_tf_template, editableCellTemplate : grid_tf_template },
		{field: 'beneficiary_id', displayName: 'Beneficiary ID', width: 120, cellTemplate : grid_tf_template, editableCellTemplate : grid_tf_template }
	];

	$scope.grid_options = {
		data: 'transaction_options',
		enableCellSelection: false,
		enableRowSelection: false,
		enableCellEditOnFocus: true,
		columnDefs: 'grid_cols_def',
		enablePaging: true
	};


	function get_select_vouchers(){
		var payload = $scope.ui;

		gen_http(
			$http,
			200,
			{
				method: 'GET',
				url: $rootScope.api_gen + '/demo/select_vouchers',
				params: { 'foobar': new Date().getTime() }
			},
			function (data, status){
				if(status){
					alert(status);;
				}
				if(data){
					alert(data);
				}
			},
			function (data, status){
				$scope.ui_vouchers = data;
			}
		);
	}
	get_select_vouchers();

	function get_select_agents(){
		var payload = $scope.ui;

		gen_http(
			$http,
			200,
			{
				method: 'GET',
				url: $rootScope.api_gen + '/demo/select_agents',
				params: { 'foobar': new Date().getTime() }
			},
			function (data, status){
				if(status){
					alert(status);;
				}
				if(data){
					alert(data);
				}
			},
			function (data, status){
				$scope.ui_agents = data;
			}
		);
	}
	get_select_agents();

	function get_select_clients(){
		var payload = $scope.ui;

		gen_http(
			$http,
			200,
			{
				method: 'GET',
				url: $rootScope.api_gen + '/demo/select_clients',
				params: { 'foobar': new Date().getTime() }
			},
			function (data, status){
				if(status){
					alert(status);;
				}
				if(data){
					alert(data);
				}
			},
			function (data, status){
				$scope.ui_clients = data;
			}
		);
	}
	get_select_clients();

	function get_select_beneficiaries(){
		var payload = $scope.ui;

		gen_http(
			$http,
			200,
			{
				method: 'GET',
				url: $rootScope.api_gen + '/demo/select_beneficiaries',
				params: { 'foobar': new Date().getTime() }
			},
			function (data, status){
				if(status){
					alert(status);;
				}
				if(data){
					alert(data);
				}
			},
			function (data, status){
				$scope.ui_beneficiaries = data;
			}
		);
	}
	get_select_beneficiaries();



});

