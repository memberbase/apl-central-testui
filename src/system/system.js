
main_controllers.controller('system_accounts_controller', function($http, $scope, $rootScope, $location, $routeParams) {
  navbar_remove_all_active();
  $("#navli_system_accounts").addClass("active");

  $scope.rs = $rootScope;
  $scope.ui = {};
  $scope.ui.sub_accounts_label = 'Sub Accounts';
  $scope.ui.system_accounts = [];
  $scope.ui.wallet_accounts = [];
  $scope.ui.agent_accounts = [];
  $scope.ui.display_accounts = [];
  $scope.ui.nostro = [];
  $scope.ui.sub_accounts = [];

  function get_data(endpoint,callback) {
    var result = {};
    gen_http(
      $http,
      200,
      {
        method: 'GET',
        url: $rootScope.api_gen + '/' + endpoint,
        params: { 'foobar': new Date().getTime() }
      },
      function (data, status){
        gen_http_error(data, status);
        assign_array = [];
        callback(null,{});
      },
      function (data, status){
        result = data;
        callback(null,result);
      }
    );
  };

  function systemAccountByNo(id){
    var a = $scope.ui.system_accounts;
    result = {};
    for(var i=0; i < a.length; i++){
      if(a[i].id == id){
        result = a[i];
      }
    }
    return result;
  }

  function add_display_account(assign_array, id, name, tx_link_yn, sub_link_yn){
    var item = systemAccountByNo(id);
    item.name = name;
    item.tx_link_yn = tx_link_yn;
    item.sub_link_yn = sub_link_yn;
    assign_array.push(item);
  }

  function init_load(){
    $scope.ui.display_accounts = [];
    $scope.ui.nostro = [];
    $scope.ui.sub_accounts = [];
    $scope.transaction_list = [];

    async.waterfall([
      function(callback) {
        get_data('system_accounts', callback);
      },
      function(data,callback) {
        $scope.ui.system_accounts = data.accounts;
        get_data('wallet_accounts', callback);
      },
      function(data,callback) {
        $scope.ui.wallet_accounts = data.accounts;
        get_data('agent_accounts', callback);
      },
      function(data,callback) {
        $scope.ui.agent_accounts = data.accounts;

        //---------------------

        var display_accounts = [];
        add_display_account(display_accounts,'100002','Deposit Settlement',true,false);
        add_display_account(display_accounts,'100006','Deposit Suspense',true,false);
        add_display_account(display_accounts,'100003','Card Purchase Settlement',true,false);
        add_display_account(display_accounts,'100004','Vouchers',true,false);

        var total_wallets = 0;
        for (var i=0; i<$scope.ui.wallet_accounts.length; i++) {
          total_wallets = total_wallets + $scope.ui.wallet_accounts[i].balance;
          $scope.ui.wallet_accounts[i].tx_link_yn = false;
        }
        var o = {};
        //o.id = '';
        o.name = 'Wallet Accounts';
        //o.account_id = '';
        o.currency = 'EUR';
        o.balance = total_wallets;
        o.tx_link_yn = false;
        o.sub_link_yn = true;
        o.sub_link = 'Wallets';
        display_accounts.push(o);

        var total_agents = 0;
        for (var i=0; i<$scope.ui.agent_accounts.length; i++) {
          total_agents = total_agents + $scope.ui.agent_accounts[i].balance;
          $scope.ui.agent_accounts[i].tx_link_yn = false;
        }
        var o = {};
        //o.id = '';
        o.name = 'Agent Cash Accounts';
        //o.account_id = '';
        o.currency = 'EUR';
        o.balance = total_agents;
        o.tx_link_yn = false;
        o.sub_link_yn = true;
        o.sub_link = 'Agents';
        display_accounts.push(o);

        add_display_account(display_accounts,'100007','Fees',true,false);

        var total = 0;
        for (var i=0; i<display_accounts.length; i++) {
          total = total + display_accounts[i].balance;
        }
        var o = {};
        //o.id = '';
        o.name = 'Total';
        //o.account_id = '';
        o.currency = 'EUR';
        o.balance = total;
        o.tx_link_yn = false;
        o.sub_link_yn = false;
        display_accounts.push(o);

        $scope.ui.display_accounts = display_accounts;

        //---------------------

        //add Nostro account
        add_display_account($scope.ui.nostro,'100001','Nostro',true,false);
        callback(null,{});
      }
    ],
    function(err,data) {
    });
  }

  init_load();
  $scope.reload_button = function () {
    init_load();
  };


  // -------------------------------------------------------
  var view_transactions_template = '<div ng-show="row.entity[\'tx_link_yn\']">';
  view_transactions_template += '<button type="button" class="btn btn-xs"';
  view_transactions_template += 'ng-click="view_transactions_button(row.entity[col.field])">';
  view_transactions_template += '<i class="fa fa-arrow-right"></i>';
  view_transactions_template += '</button>';
  view_transactions_template += '</div>';

  $scope.view_transactions_button = function(aid){  
    get_transaction(aid);
  };

  var view_sub_accounts_template = '<div ng-show="row.entity[\'sub_link_yn\']">';
  view_sub_accounts_template += '<button type="button" class="btn btn-xs"';
  view_sub_accounts_template += 'ng-click="view_sub_accounts_button(row.entity[\'sub_link\'])">';
  view_sub_accounts_template += '<i class="fa fa-arrow-right"></i>';
  view_sub_accounts_template += '</button>';
  view_sub_accounts_template += '</div>';

  $scope.view_sub_accounts_button = function(link){  
    if(link == 'Wallets'){
      $scope.ui.sub_accounts = $scope.ui.wallet_accounts;
      $scope.ui.sub_accounts_label = 'Wallet Sub Accounts';
    }
    if(link == 'Agents'){
      $scope.ui.sub_accounts = $scope.ui.agent_accounts;
      $scope.ui.sub_accounts_label = 'Cash Agent Sub Accounts';
    }
  };


  $scope.grid_cols_def = [
    {field: 'id', displayName: 'ID', width: 80},
    {field: 'account_no', displayName: 'Account No', width: 120},
    {field: 'currency', displayName: 'Currency', width: 100},
    {field: 'balance', displayName: 'Balance', width: 80, cellFilter : 'customCurrency : "" : 2', cellClass: 'currency-align'},
    {field: 'name', displayName: 'Name', width: 200},
    {field: 'id', displayName: 'TXs', width: 50, cellTemplate : view_transactions_template, editableCellTemplate : view_transactions_template },
    {field: 'id', displayName: 'Sub', width: 50, cellTemplate : view_sub_accounts_template, editableCellTemplate : view_sub_accounts_template }
  ];

  $scope.grid_cols_def_b = [
    {field: 'id', displayName: 'ID', width: 80},
    {field: 'account_no', displayName: 'Account No', width: 120},
    {field: 'currency', displayName: 'Currency', width: 100},
    {field: 'balance', displayName: 'Balance', width: 80, cellFilter : 'customCurrency : "" : 2', cellClass: 'currency-align'},
    {field: 'name', displayName: 'Name', width: 200},
    {field: 'id', displayName: 'TXs', width: 50, cellTemplate : view_transactions_template, editableCellTemplate : view_transactions_template }
  ];


  $scope.grid_system_accounts_opt = {
    data: 'ui.display_accounts',
    enableCellSelection: false,
    enableRowSelection: false,
    enableCellEditOnFocus: true,
    columnDefs: 'grid_cols_def',
    enablePaging: true
  };

  $scope.grid_nostro_opt = {
    data: 'ui.nostro',
    enableCellSelection: false,
    enableRowSelection: false,
    enableCellEditOnFocus: true,
    columnDefs: 'grid_cols_def_b',
    enablePaging: true
  };

  $scope.grid_sub_accounts_opt = {
    data: 'ui.sub_accounts',
    enableCellSelection: false,
    enableRowSelection: false,
    enableCellEditOnFocus: true,
    columnDefs: 'grid_cols_def_b',
    enablePaging: true
  };


  // -------------------------------------------------------
  $scope.transaction_records = [];
  $scope.transaction_list = [];

  $scope.grid_cols_def2 = [
    {field: 'id', displayName: 'ID', width: 80},
    {field: 'type', displayName: 'Type', width: 200},
    {field: 'amount', displayName: 'Balance', width: 80, cellFilter : 'customCurrency : "" : 2', cellClass: 'currency-align'},
    {field: 'time', displayName: 'Time', width: 200, cellFilter : 'date : "yyyy-MM-dd HH:mm:ss" : "+0200"'}
  ];

  $scope.grid_transactions_opt = {
    data: 'transaction_list',
    enableCellSelection: false,
    enableRowSelection: false,
    enableCellEditOnFocus: true,
    columnDefs: 'grid_cols_def2',
    enablePaging: true
  };

  function get_transaction(aid){
    gen_http(
      $http,
      200,
      {
        method: 'GET',
        url: $rootScope.api_gen + '/accounts/' + aid + '/transactions',
        params: { 'foobar': new Date().getTime() }
      },
      function (data, status){
        gen_http_error(data, status);
      },
      function (data, status){
        $scope.transaction_records = {};
        $scope.transaction_list = [];
        if(data.records){
          $scope.transaction_records = data.records;
        }
        if(data.records.data){
          $scope.transaction_list = data.records.data;
        }
      }
    );
  }


});

