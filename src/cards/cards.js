
main_controllers.controller('cards_controller', function($http, $scope, $rootScope, $location) {
  navbar_remove_all_active();
  $("#navli_cards").addClass("active");

  $scope.rs = $rootScope;
  $scope.ui = {};
  $scope.card_records = {};
  $scope.card_list = [{rowitem:1}];

  function get_cards() {
    gen_http(
      $http,
      200,
      {
        method: 'GET',
        url: $rootScope.api_gen + '/cards', //+ '?offset=' + $scope.list_page,
        params: { 'foobar': new Date().getTime() }
      },
      function (data, status){
        gen_http_error(data, status);
      },
      function (data, status){
        $scope.card_records = {};
        $scope.card_list = [];
        if(data.records){
          $scope.card_records = data.records;
        }
        if(data.records.data){
          $scope.card_list = data.records.data;
        }
      }
    );
  }
  get_cards();

  $scope.grid_cols_def = [
    {field: 'id', displayName: 'ID', width: 80},
    {field: 'profile_id', displayName: 'Profile ID', width: 130},
    {field: 'account_no', displayName: 'Card Number', width: 200},
    {field: 'balance', displayName: 'Balance', width: 130, cellFilter : 'customCurrency : "" : 2', cellClass: 'currency-align'},
    {field: 'mobile_number', displayName: 'Mobile Sent', width: 150},
  ];

  $scope.grid_options = {
    data: 'card_list',
    enableCellSelection: false,
    enableRowSelection: false,
    enableCellEditOnFocus: true,
    columnDefs: 'grid_cols_def',
    enablePaging: true
  };

  $scope.reload_button = function(pid){  
    get_cards();
  };
})



